-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.2.22-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for desertkart
CREATE DATABASE IF NOT EXISTS `desertkart` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `desertkart`;

-- Dumping structure for table desertkart.cart
CREATE TABLE IF NOT EXISTS `cart` (
  `cartId` int(11) NOT NULL AUTO_INCREMENT,
  `productId` int(11) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`cartId`),
  KEY `productId` (`productId`),
  KEY `username` (`username`),
  CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`),
  CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table desertkart.cart: ~3 rows (approximately)
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` (`cartId`, `productId`, `username`, `quantity`) VALUES
	(8, 2, 'aysha', 7),
	(9, 2, 'aysha', 10),
	(11, 1001, 'aysha', 100);
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;

-- Dumping structure for table desertkart.category
CREATE TABLE IF NOT EXISTS `category` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`categoryId`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table desertkart.category: ~4 rows (approximately)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`categoryId`, `name`) VALUES
	(3, 'Automobile'),
	(4, 'Clothing'),
	(1, 'Electronics'),
	(2, 'Hygeine');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Dumping structure for table desertkart.product
CREATE TABLE IF NOT EXISTS `product` (
  `productId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  `categoryId` int(11) NOT NULL,
  PRIMARY KEY (`productId`),
  KEY `category_id` (`categoryId`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `category` (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=123459 DEFAULT CHARSET=latin1;

-- Dumping data for table desertkart.product: ~15 rows (approximately)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`productId`, `name`, `description`, `quantity`, `price`, `categoryId`) VALUES
	(2, 'Facewash', 'dadsadada', 100, 23, 2),
	(102, 'Iphone', 'Iphone 15', 123, 200000, 1),
	(1001, 'Mahindra Thar', 'A vehicle to be owned', 100, 1234000, 3),
	(1003, 'Smartphone', 'A high-end smartphone with advanced features', 50, 800, 1),
	(1005, 'Toothbrush', 'An electric toothbrush for effective oral hygiene', 20, 50, 2),
	(1006, 'Hand Sanitizer', 'A disinfectant hand sanitizer for germ-free hands', 200, 10, 2),
	(1007, 'Denim Jeans', 'A stylish pair of denim jeans for everyday wear', 30, 60, 3),
	(1008, 'T-Shirt', 'A comfortable cotton T-shirt for casual wear', 50, 20, 3),
	(1009, 'Car Seat Cover', 'A high-quality car seat cover for improved comfort and style', 10, 100, 4),
	(1010, 'Car Vacuum Cleaner', 'A powerful car vacuum cleaner for thorough cleaning', 5, 150, 4),
	(1011, 'Smart TV', 'An advanced smart TV with a high-resolution display', 15, 1000, 1),
	(1012, 'Fitness Tracker', 'A smart fitness tracker for tracking daily activity and health metrics', 25, 80, 1),
	(10011, 'sample', 'this is a sample product', 111, 12000, 3),
	(12345, 'sample', 'this is a sample product', 111, 12000, 3),
	(123458, 'sample', 'this is a sample product', 111, 12000, 4);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping structure for table desertkart.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `roleId` int(11) NOT NULL,
  `roleName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table desertkart.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`roleId`, `roleName`) VALUES
	(1, 'admin'),
	(2, 'user');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table desertkart.users
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `roleId` int(11) DEFAULT NULL,
  PRIMARY KEY (`username`),
  KEY `roleId` (`roleId`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `roles` (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table desertkart.users: ~9 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`username`, `email`, `password`, `roleId`) VALUES
	('Adil', 'adil@gmail.com', '12345', 2),
	('aysha', 'aysha@gmail.com', '12345', 2),
	('aysha1', NULL, '12345', 2),
	('rishi', 'rishi@gmail.com', '12345', 2),
	('rishi11', 'rishi@gmail.com11', '1234511', 2),
	('saptarshi', 'sap18das', 'sap12345', 2),
	('shabeer', 'shabeer@gmail.com', '12345', 1),
	('srihari', 'srihari@gmail.com', '12345', 2),
	('vinya', 'vinya@gm', '123', 2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
